# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: resume.pdf all clean

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: resume.pdf

resume.pdf:
	latexmk -pdf -xelatex -interaction=nonstopmode -use-make

clean:
	latexmk -CA
